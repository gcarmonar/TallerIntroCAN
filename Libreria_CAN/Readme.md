# Librería

### Descripción
La librería en la cual se basan los ejemplos fue publicada por Seedstudio

### Procedimiento
1. Visita la pagina [Github de Seed-Studio](https://github.com/Seeed-Studio/CAN_BUS_Shield) para obtener la ultima versión de la librería.
2. Descarga la librería a tu computadora dando clic en el botón __Clone or download__
3. Descomprime la librería en el directorio de librerías de Arduino (ejemplo: "/home/USERNAME/Arduino/libraries") y reinicia el IDE de Arduino (si estaba abierto)
3.1. Asegurate que el nombre de la carpeta sea acorde al nombre de la librería
4. Deberás tener la librería CAN_BUS dentro del menú _Sketch >> Include Library_