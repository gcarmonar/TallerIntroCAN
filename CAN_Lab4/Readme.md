# Laboratorio 4
## Filtros CAN

### Objetivo
Aplicar filtros en una red CAN.

### Material
Necesitaras el siguiente material para la practica:
* [2] Arduino UNO o similar
* [2] MCP2551 CAN Transceiver
* [2] MCP2515 CAN Controller
* [2] Cristales de 10 MHz
* [3] Resistencias de 100 Kohms
* [1] Resistencia de 120 ohms

### Procedimiento
1. Conecta según el diagrama __DiagramaGeneralCAN.pdf__
2. En uno de los Arduinos descarga el programa __Examples >> CAN_BUS_Shield >> set_mask_filter_send__ 
3. En el otro Arduino descarga el programa __Examples >> CAN_BUS_Shield >> set_mask_filter_recv__
4. Abre la terminal del Arduino que tiene el programa __set_mask_filter_recv__
5. Deberas de ver los mensajes CAN filtrados en la terminal (solo IDs del 0x04 al 0x09).