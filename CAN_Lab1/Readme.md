# Laboratorio 1
## Introducción a CAN

### Objetivo
Enviar un simple mensaje de un nodo a otro. Esto nos ayudara a comprobar que las conexiones electrónicas son adecuadas.

### Material
Necesitaras el siguiente material para la practica:
* [2] Arduino UNO o similar
* [2] MCP2551 CAN Transceiver
* [2] MCP2515 CAN Controller
* [2] Cristales de 16 MHz
* [3] Resistencias de 100 Kohms
* [1] Resistencia de 120 ohms

### Procedimiento
1. Conecta según el diagrama __DiagramaGeneralCAN.pdf__
2. En uno de los Arduinos descarga el programa __Examples >> CAN_BUS_Shield >> send__
3. En el otro Arduino descarga el programa __Examples >> CAN_BUS_Shield >> receive_check__
4. Abre la terminal del Arduino que tiene el programa __receive_check__
5. Deberás de ver los mensajes recibidos por CAN en la terminal.
