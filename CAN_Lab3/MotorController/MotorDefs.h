
#pragma once

// General definitions
#define DEBUG_ON		1	// Set to Zero to disable DEBUG serial messages

// CAN definitions
#define MASTER_CTRL_ID	0X88
#define MOTOR_CTRL_ID	0X99
#define SEND_CAN_PERIOD 500 // In milliseconds

// C types redefined
#define BOOL 	bool
#define UInt8	byte
#define SInt8	char
#define UInt16	unsigned int
#define SInt16	int
#define UInt32 	unsigned long
#define SInt32	long
#define Single 	float

// I/O
#define SPI_CS_PIN      10
#define TEMP_PIN        A0
#define MOTOR_PIN       6

// Mode status
#define SPD_CTRL_MODE   0x01
#define OFF_MODE        0x00

 
// Variables
UInt8 mode = OFF_MODE;
Single actualTemp = 0;
Single maxTemp;
UInt16 maxSpeed = 0;
UInt16 actualSpeed = 0;
UInt16 reqSpeed = 0;
UInt32 lastSentMsg = 0;


 // Received from MasterMCU
void SetMode(UInt8 data){		mode = data;		}

void SetReqSpeed(UInt16 data){	reqSpeed = data;	}

void SetMaxTemp(Single data){	maxTemp = data;		}

void SetMaxSpeed(UInt16 data){	maxSpeed = data;	}

void SetActSpeed(UInt16 data){  actualSpeed = data;	}   // Added


// Sent to MasterMCU
UInt8 GetMode(void){		return mode;		}

Single GetMaxTemp(void){	return maxTemp;		}

Single GetActTemp(void){	return actualTemp;	}

UInt16 GetActSpeed(void){	return actualSpeed;	}

UInt16 GetMaxSpeed(void){	return maxSpeed;	}

// Other functions
UInt16 GetReqSpeed(void){	return reqSpeed;	}