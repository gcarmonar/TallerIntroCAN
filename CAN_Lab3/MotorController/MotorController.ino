/**
	MotorControler.ino
	Bi-directional communication example. 

	Temperature sensor:
		* Min temp: -55 °C
		* Max temp: 120 °C
		* Sensor output: 20 mV/°C, for 0 °C output is 1500 mV.

	Motor Specs
		* Max Speed: 6000 RPMs
		* Maximum operational ambient temperature 90°C
		* Spins only in one direction

	@author		Gerardo Carmona
	@version	1.0 First release
	@date 		9/23/2017

*/

#include <mcp_can.h>
#include "MotorDefs.h"


MCP_CAN CAN(SPI_CS_PIN);


void setup(){
#if DEBUG_ON
	Serial.begin(115200);
#endif
	
	pinMode(MOTOR_PIN, OUTPUT);
	pinMode(TEMP_PIN, INPUT);// Not necessary

	while (CAN_OK != CAN.begin(CAN_500KBPS))
    {
#if DEBUG_ON
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
#endif
        delay(100);
    }

#if DEBUG_ON
    Serial.println("CAN BUS Shield init ok!");
#endif
}

void loop(){
	// ADD CODE: Check if we have CAN Msgs, if true read message, then check 
	// mod status and update parameters
	
	
    // Try to send CAN messages every ~SEND_CAN_PERIOD ms
	if ((millis()-lastSentMsg) >= SEND_CAN_PERIOD){
        lastSentMsg = millis();	
		SendCan();
	}
}


/* ------------------------------------------------------------------
 *  Function: GetTemp
 *
 *  Description:
 *  Reads ADC input and converts value to °C
 *
 *  Parameters:
 *
 *  Return: [Single precision] Temperature in °C
 * --------------------------------------------------------------- */
Single GetTemp(void){
	// ADD CODE: Read the analog pinMode
	// Convert to engineering units
	// Print raw value from the ADC and temperature in °C
	// Return temperature
	

#if DEBUG_ON
	Serial.print("Raw value: ");
	Serial.print(rawValue);
	Serial.print(", Temp °C: ");
	Serial.println(TempC);
#endif

	return TempC;
}


/* ------------------------------------------------------------------
 *  Function: SetSpeed
 *
 *  Description:
 *  Sets the PWM to the Motor Output
 *
 *  Parameters: [UInt16] speed for the motor in RPMs
 *
 *  Return:
 * --------------------------------------------------------------- */
void SetSpeed(UInt16 speed){
	int pwm;

	if (speed >= 0 && speed < 6000){
		// Transform to PWM
		pwm = map(speed, 0, 6000, 0, 255);
		analogWrite(MOTOR_PIN, pwm);

		SetActSpeed(speed); // Added

#if DEBUG_ON
		Serial.print("Motor PWM: ");
		Serial.println(pwm);
#endif	
	}
}


/* ------------------------------------------------------------------
 *  Function: GetCAN
 *
 *  Description:
 *  Function to get the CAN message and sets the global vars
 *
 *  Parameters:
 *
 *  Return:
 * --------------------------------------------------------------- */
void GetCAN(void){
	unsigned char buf[8];
	unsigned char len=0;
	unsigned int canId=0;

	// ADD CODE: Read buffer and get CAN ID. Verify that ID matches
	// Master Controller, if not discard.

#if DEBUG_ON
	Serial.print("CAN ID: ");
	Serial.println(canId, HEX);
#endif

#if DEBUG_ON	// Print message
	Serial.print("Received CAN data: ");
	for(int i = 0; i<len; i++){
		Serial.print(buf[i], HEX);
		Serial.print("\t");
	}
	Serial.println();
#endif

	SetMode(buf[0]);
#if DEBUG_ON
	Serial.print("Mode request: ");
	Serial.println(buf[0]);
#endif	
	// ADD CODE: Get value and convert to engineering units (apply proper  
	// factor and offset values)
	
#if DEBUG_ON
	Serial.print("Temp limit [C]: ");
	Serial.println(GetMaxTemp());
#endif
	// Get value and convert to engineering units
	// Value is sent in RPMs with no scale and no offset
	SetMaxSpeed((buf[4] << 8) | buf[5]);		
#if DEBUG_ON
	Serial.print("Speed limit: ");
	Serial.println(GetMaxSpeed());
#endif
	// ADD CODE: Get value and convert to engineering units
	
#if DEBUG_ON
	Serial.print("Speed request: ");
	Serial.println(GetReqSpeed());
#endif
}


/* ------------------------------------------------------------------
 *  Function: SendCAN
 *
 *  Description:
 *  Function to send CAN message to Master
 *
 *  Parameters:
 *
 *  Return:
 * --------------------------------------------------------------- */
void SendCan(void){
	UInt8 stmp[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	UInt16 temp;
	UInt16 spd;

    // ADD CODE: Get temperature value and apply offset and factor

	
	// No conversion needed, because the variable holds the value from 0 to 65535
	// and factor = 1 and offset = 0. There is a separated function that 
	// transforms to motor value
	spd = GetActSpeed();

	// ADD CODE: Build message
	

#if DEBUG_ON	// Print message
	Serial.print("Send CAN data: ");
	for(int i = 0; i<8; i++){
		Serial.print(stmp[i], HEX);
		Serial.print("\t");
	}
	Serial.println();
#endif

	// ADD CODE: Send message
	
}