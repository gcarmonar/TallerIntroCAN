# Laboratorio 3
## Comunicación Bidireccional

### Objetivo
Implementar comunicación bidireccional en una red CAN. 

## Descripción
Un Arduino emulara el control de un motor (por medio de un led) y su temperatura (simulado con un potenciómetro). El otro Arduino será el controlador maestro quien recibirá el estado actual del controlador del motor y decidirá que acciones debe tomar. El controlador maestro tendrá también una entrada digital (pushbutton) que definirá el estado del motor y otra entrada que para la velocidad deseada (mediante un potenciómetro)

### Limitaciones
* El motor solo gira en un sentido

### Material
Necesitaras el siguiente material para la practica (Ver diagrama Lab3):
* [2] Arduino UNO o similar
* [2] MCP2551 CAN Transceiver
* [2] MCP2515 CAN Controller
* [2] Cristales de 10 MHz
* [3] Resistencias de 100 Kohms
* [1] Resistencia de 120 ohms
* [1] Led 5 mm cualquier color
* [1] Resistencia entre 220 y 330 ohms
* [2] Potenciómetro de 10 Kohms (entre 5 y 50 Kohms)
* [1] Pushbutton

### Requerimientos
1. Conecta según el diagrama CAN_Lab3.pdf
1. El led y potenciómetro simularan un motor y un sensor de temperatura respectivamente.
2. El switch simulara una entrada para encender/apagar el motor (estatus) y el potenciómetro para solicitar velocidad deseada.
3. Cuando se presiona el switch en el Nodo 1 (controlador maestro) enviara la instrucción de encender el motor al Nodo 2 (controlador del motor). Nodo 1 enviara también la velocidad solicitada al nodo 2.
4. El Nodo 2 deberá responder con el Modo de operación en el cual se encuentra (0 - Detenido, 1 - Encendido) y la temperatura del motor al Nodo 1.
5. Los detalles de la comunicación CAN se especifican en el documento __Mensajes_CAN.xlsx__