#pragma once

// General definitions
#define DEBUG_ON		1	// Set to Zero to disable DEBUG serial messages

// CAN definitions
#define MASTER_CTRL_ID	0X88
#define MOTOR_CTRL_ID	0X99
#define SEND_CAN_PERIOD 500 // In milliseconds

#define BOOL 	bool
#define UInt8	byte
#define SInt8	char
#define UInt16	unsigned int
#define SInt16	int
#define UInt32 	unsigned long
#define SInt32	long
#define Single 	float

// I/O
#define SPI_CS_PIN		10
#define MAX_TEMP_PIN    A2
#define MAX_SPD_PIN		A1
#define SPD_REQ_PIN     A0
#define MODE_REQ_PIN    5

// Mode status
#define SPD_CTRL_MODE	1
#define OFF_MODE		0


UInt8 reqMode       = OFF_MODE;
UInt16 reqSpeed     = 0;
UInt16 reqMaxSpeed  = 0;
Single reqMaxTmp    = 0;
UInt32 lastSentMsg  = 0;


// Set Functions
void SetReqMode(UInt8 var){      reqMode = var;      }
void SetReqSpeed(UInt16 var){    reqSpeed = var;     }
void SetMaxSpeed(UInt16 var){    reqMaxSpeed = var;  }
void SetReqMaxTmp(Single var){   reqMaxTmp = var;    }

// Get Functions
UInt8 GetReqMode(void){      return reqMode;      }
UInt16 GetReqSpeed(void){    return reqSpeed;     }
UInt16 GetMaxSpeed(void){    return reqMaxSpeed;  }
Single GetReqMaxTmp(void){   return reqMaxTmp;    }
