/**
	MasterControler.ino
	Bi-directional communication example. 

	Temperature sensor:
		* Min temp: -55 °C
		* Max temp: 120 °C
		* Sensor output: 20 mV/°C, for 0 °C output is 1500 mV.

	Motor Specs
		* Max Speed: 6000 RPMs
		* Maximum operational ambient temperature 90°C
		* Spins only in one direction

	@author		Gerardo Carmona
	@version	1.0 First release
	@date 		9/23/2017

*/

#include <mcp_can.h>
#include "MasterDefs.h"


MCP_CAN CAN(SPI_CS_PIN);


void setup(){
#if DEBUG_ON
	Serial.begin(115200);
#endif

	// i/o
	pinMode(MAX_TEMP_PIN, INPUT);
	pinMode(SPD_REQ_PIN, INPUT);
	pinMode(MAX_SPD_PIN, INPUT);
	pinMode(MODE_REQ_PIN, INPUT_PULLUP);

	while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : baudrate = 500k
    {
#if DEBUG_ON
        Serial.println("CAN BUS Shield init fail");
        Serial.println(" Init CAN BUS Shield again");
#endif

        delay(100);
    }

#if DEBUG_ON
    Serial.println("CAN BUS Shield init ok!");
#endif
}


void loop(){
	// Read inputs
	UpdateInputs();

	// Send CAN msg
	if ((millis()-lastSentMsg) >= SEND_CAN_PERIOD){
        lastSentMsg = millis();	
		SendCan();
	}

	// Receive CAN msg
	if(CAN_MSGAVAIL == CAN.checkReceive()){
		GetCAN();  // We don't use the feedback for anything yet
	}

#if DEBUG_ON
	delay(100);
#endif

}


/* ------------------------------------------------------------------
 *  Function: SendCAN
 *
 *  Description:
 *  Function to send CAN message to Motor Controller
 *
 *  Parameters:
 *
 *  Return:
 * --------------------------------------------------------------- */
void SendCan(){
	// ADD CODE: Get Values and create message

#if DEBUG_ON	// Print message
	Serial.print("Send CAN data: ");
	for(int i = 0; i<8; i++){
		Serial.print(stmp[i], HEX);
		Serial.print("\t");
	}
	Serial.println();
#endif

	// ADD CODE: Send message
	
}


/* ------------------------------------------------------------------
 *  Function: GetCAN
 *
 *  Description:
 *  Function to get the CAN message feedback from the Motor Controller
 *
 *  Parameters:
 *
 *  Return:
 * --------------------------------------------------------------- */
void GetCAN(void){
	unsigned char buf[8];
	unsigned char len=0;
	unsigned int canId=0;

	CAN.readMsgBuf(&len, buf);
	canId = CAN.getCanId();
#if DEBUG_ON
	Serial.print("CAN ID: ");
	Serial.println(canId, HEX);
#endif

	if (canId == MOTOR_CTRL_ID){
#if DEBUG_ON	// Print message
		Serial.print("Received CAN data: ");
		for(int i = 0; i<len; i++){
			Serial.print(buf[i], HEX);
			Serial.print("\t");
		}
		Serial.println();
#endif

	}	
}


/* ------------------------------------------------------------------
 *  Function: UpdateInputs
 *
 *  Description:
 *  Function to read all Inputs from user
 *
 *  Parameters:
 *
 *  Return:
 * --------------------------------------------------------------- */
void UpdateInputs(void){

    if (!digitalRead(MODE_REQ_PIN)){
    	SetReqMode(SPD_CTRL_MODE);
    }else{
    	SetReqMode(OFF_MODE);
    }
#if DEBUG_ON
    Serial.print("RAW Req mode: ");
    Serial.println(digitalRead(MODE_REQ_PIN));
    Serial.print("Req mode: ");
    Serial.println(GetReqMode());
#endif

	// Update Speed Request upon analog input
    SetReqSpeed(map(analogRead(SPD_REQ_PIN),0,1023,0,6000));
#if DEBUG_ON
    Serial.print("RAW Req Speed: ");
    Serial.println(analogRead(SPD_REQ_PIN));
    Serial.print("Req Speed: ");
    Serial.println(GetReqSpeed());
#endif

    // Update Max Speed upon analog input
    SetMaxSpeed(map(analogRead(MAX_SPD_PIN),0,1023,0,6000));
#if DEBUG_ON
    Serial.print("RAW Max Speed: ");
    Serial.println(analogRead(MAX_SPD_PIN));
    Serial.print("Max Speed: ");
    Serial.println(GetMaxSpeed());
#endif

	// Update max temperature. Motor operational range up to 90 °C and
	// This transformation is really close to motor operational range
    SetReqMaxTmp(((Single)analogRead(MAX_TEMP_PIN)/9) - 25.0);
#if DEBUG_ON
    Serial.print("RAW Max Temp: ");
    Serial.println(analogRead(MAX_TEMP_PIN));
    Serial.print("Max temp: ");					// Corrected String
    Serial.println(GetReqMaxTmp());
#endif
}