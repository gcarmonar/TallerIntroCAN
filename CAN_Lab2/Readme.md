# Laboratorio 2
## CAN e Interrupciones

### Objetivo
Realizar una interrupción al recibir un mensaje CAN.

### Material
Necesitaras el siguiente material para la practica:
* [2] Arduino UNO o similar
* [2] MCP2551 CAN Transceiver
* [2] MCP2515 CAN Controller
* [2] Cristales de 16 MHz
* [3] Resistencias de 100 Kohms
* [1] Resistencia de 120 ohms

### Procedimiento
1. Conecta según el diagrama __DiagramaGeneralCAN.pdf__
2. Reemplaza el programa __Receive_Check__ por el siguiente: __Examples >> CAN_BUS_Shield >> receive_interrupt__
3. Abre la terminal después de haber programado para ver el resultado (mismo mensaje recibido pero ahora por medio de interrupciones)